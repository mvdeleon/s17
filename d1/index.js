
	

	console.log("Hello World");

// Function declarations

	function printName() {
	console.log("My name is John.")
	};

printName();

// Function Invocation

	printName();
	declaredFunction();

	function declaredFunction() {
		console.log("Hello World from declaredFunction.")
	}

	declaredFunction();

// Function Expression

	let variableFunction = function () {
		console.log("Hello Again.");
	};

	variableFunction();

	let funcExpression = function funcName() {
		console.log("Hello From the Other Side");
	};

	funcExpression();

	declaredFunction = function(){
	console.log("Updated declaredFunction");
	};

	declaredFunction();

	funcExpression = function(){
	console.log("Updated funcExpression");
	};

	funcExpression();

	// const constantFunc = function(){
	// 	console.log("Initialized with Const!");

	// };

	// constantFunc = function() {
	// 	console.log("Cannot be re-assigned!");
	// };

	// constantFunc();


// Function Scoping

	// {

	// 	let localVar = "Armando Perez";
	// 	console.log(localVar);

	// }

	// let globalVar = "Mr. Worldwide";
	// console.log(globalVar);

// Function Scope

	function showNames() {

		var functionVar ="Joe";
		const functionConst = "John";
		let functionLet = "Jane";

		console.log(functionVar);
		console.log(functionConst);
		console.log(functionLet);




	};

	showNames();

	// Nested Function

	function myNewFunction () {
		let name = "Cee";

		function nestedFunction() {
			let nestedName = "Thor";
			console.log(name);
			console.log(nestedName);
		}
		// console.log(nestedName);
		
		nestedFunction();
	};

	myNewFunction();
	// nestedFunction();

	// Function and Global Scoped Variables

	let globalName = "Nej";

	function myNewFunction2() {
		let nameInside = "Martin";
		console.log(globalName);
	}

	myNewFunction2();


//  Using Alert | alert();

	alert("Hello World");

	function showSampleAlert() {
		alert("Hello User");
	};

	showSampleAlert();


	console.log("I will only log in the console when the alert is dismissed.");

// Using Prompt

	let samplePrompt = prompt ("Enter your Name.");
	console.log("Hello, " + samplePrompt);

	let sampleNullPrompt = prompt("Don't enter anything");
	console.log(sampleNullPrompt);

// Function Naming conventions

	function getCourses() {
		let courses = ["Science 101", "Math 101", "English 101"];
		console.log(courses);
	}

	getCourses();