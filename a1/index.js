	// console.log("Hello World");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function personDetails() {
		let person1 = prompt("What is your name?");
		console.log("Hello " + person1);
		let age = prompt("How old are you?");
		console.log("You are " + age + " years old.");
		let location = prompt("Where do you live?");
		console.log("You live in " + location);
	};

	personDetails();
	


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.


	
*/
	function bandName() {
		console.log("1. Up Dharma Down");
		console.log("2. Bamboo");
		console.log("3. Eraserheads");
		console.log("4. Parokya Ni Edgar");
		console.log("5. Kamikazee");
	};


	bandName();

	//second function here:

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	function displayMovies() {

		console.log("1. Forrest Gump");
		console.log("Rotten Tomatoes Rating: 71%");

		console.log("2. Harry Potter series");
		console.log("Rotten Tomatoes Rating: 96%");

		console.log("3. Blast From The Past");
		console.log("Rotten Tomatoes Rating: 58%");

		console.log("4. Avengers: End Game");
		console.log("Rotten Tomatoes Rating: 94%");

		console.log("5. Legally Blonde");
		console.log("Rotten Tomatoes Rating: 71%");

	};

	displayMovies();
	
	
	//third function here:

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


	
	let printFriends = function printUsers()
	{
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();

// console.log(friend1);
// console.log(friend2);